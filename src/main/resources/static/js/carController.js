var app = angular.module('myApp', []);
app.controller('getAllCarsCtrl', function($scope, $http) {
    $http({
        method : "GET",
        url : "http://localhost:7777/springsecurity/car/getall"
    }).then(function mySuccess(response) {
        $scope.cars = response.data;
    }, function myError(response) {
        $scope.cars = response.statusText;
    });
});

app.controller('addCarCtrl', function($scope, $http) {
    $scope.sendData = function () {
        /*var personData = {
            personId : $scope.personId
        }*/
        var carData = {
            carName : $scope.carName,
            person : {
                personId : $scope.personId
            }
        };
        $http({
            method : "POST",
            url : "http://localhost:7777/springsecurity/car/add",
            data : carData
        }).then(function mySuccess(response) {
            $scope.message = 'Successfully added a car with id : ' + response.data + ' and status : ' + response.status;
        }, function myError(response) {
            $scope.message = response.statusText;
        });
    }
});