package com.app.springsecurity.entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "User_details")
@Data
public class User {

    @Id
    @GeneratedValue
    @Column(unique = true, nullable = false)
    private Integer userId;

    @Column(unique = true, nullable = false, length=20)
    private String userName;

    @Column(nullable = false, length=100)
    private String password;

    @Column(nullable = false, length=30)
    private String role;
}
