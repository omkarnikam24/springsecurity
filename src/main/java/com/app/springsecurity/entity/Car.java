package com.app.springsecurity.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "Car")
@Data
@ToString(exclude= {"person"})
@EqualsAndHashCode(exclude= {"person"})
public class Car {

	@Id
	@GeneratedValue
	@Column(unique = true, nullable = false)
	private Integer carId;
	
	@Column(unique = false, nullable = false, length=50)
	private String carName;
	
	@ManyToOne
	@JoinColumn( name = "personId", nullable = false )
	private Person person;
	
	/*@ManyToOne( cascade=CascadeType.ALL, fetch = FetchType.LAZY ) 
	@JoinColumn( name = "personId")
	private Person person;*/

}
