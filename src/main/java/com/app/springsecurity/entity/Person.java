package com.app.springsecurity.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "Person")
@Data
//@ToString(exclude= {"cars"})
@EqualsAndHashCode(exclude= {"cars"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "personId")
public class Person {

	@Id
	@GeneratedValue
	@Column(unique = true, nullable = false)
	private Integer personId;
	
	@Column(unique = false, nullable = false, length=50)
	private String name;
	
	@Column(unique = false, nullable = true, length=100)
	private String address;
	
	/*@OneToMany( cascade = CascadeType.ALL , fetch = FetchType.EAGER, orphanRemoval = true )
	@JoinColumn( name = "personId", referencedColumnName = "personId")
	private List<Car> cars;*/
	
	@OneToMany( mappedBy = "person" , cascade = CascadeType.ALL, orphanRemoval = false, fetch = FetchType.EAGER )
	@JsonIgnore
	private List<Car> cars;
	
}
