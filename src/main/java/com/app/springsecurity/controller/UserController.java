package com.app.springsecurity.controller;

import com.app.springsecurity.entity.User;
import com.app.springsecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService service;

    @PostMapping(value = "/add", produces = "application/json")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        //logger.info("Object ->"+brand);
        System.out.println("In User Add");
        return service.addUser(user);
    }

    @PostMapping(value = "/addFirstUser", produces = "application/json")
    public ResponseEntity<?> addFirstUser(@RequestBody User user) {
        //logger.info("Object ->"+brand);
        System.out.println("In Add First User");
        return service.addFirstUser(user);
    }

    @GetMapping(value = "/get/{id}", produces = "application/json")
    public ResponseEntity<?> getUser(@PathVariable Integer id) {
        //logger.info("Id to be deleted -> "+id);
        return service.getUser(id);
    }

    @GetMapping(value = "/getall", produces = "application/json")
    public ResponseEntity<?> getAllUsers() {
        return service.getAllUsers();
    }

}
