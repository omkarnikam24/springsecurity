package com.app.springsecurity.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;
import com.app.springsecurity.entity.Car;
import com.app.springsecurity.entity.Person;
import com.app.springsecurity.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    PersonService service;

    @Autowired
    ApplicationContext appContext;

    @PostMapping(value = "/add", produces = "application/json")
    public ResponseEntity<?> addPerson(@RequestBody Person person) {
        //logger.info("Object ->"+brand);
        System.out.println("In Person Add");
        return service.addPerson(person);
    }

    @GetMapping(value = "/get/{id}", produces = "application/json")
    public ResponseEntity<?> getPerson(@PathVariable Integer id) {
        //logger.info("Id to be deleted -> "+id);
        return service.getPerson(id);
    }

    @GetMapping(value = "/getall", produces = "application/json")
    public ResponseEntity<?> getAllPersons() {
        return service.getAllPersons();
    }

}
