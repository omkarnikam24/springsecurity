package com.app.springsecurity.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.springsecurity.entity.Car;
import com.app.springsecurity.service.CarService;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/car")
public class CarController {
	
	@Autowired 
	CarService carService;

	@PostMapping(value = "/add", produces = "application/json")
	public ResponseEntity<?> addCar(@RequestBody Car car){
		System.out.println("In add car POST, car : " + car);
		if(null != car)
			return carService.addCar(car);
		return null;
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")
	public ResponseEntity<?> getCar(@PathVariable Integer id){	
		return carService.getCar(id);
	}


	@GetMapping(value = "/allCars", produces = "application/json")
	public ModelAndView allCarsView(){
		return new ModelAndView("cars");
	}

	@GetMapping(value = "/addCar", produces = "application/json")
	public ModelAndView addCarView(){
		return new ModelAndView("addCar");
	}
	@GetMapping(value = "/getall", produces = "application/json")
	public ResponseEntity<?> getAllCars(){
		System.out.println("In get all Cars");
		return carService.getAllCars();
	}
	
}
