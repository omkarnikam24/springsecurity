package com.app.springsecurity.controller;

import com.app.springsecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
//@RequestMapping("/")
public class GlobalController {

    @Autowired
    UserService service;

    @GetMapping(value = "/login", produces = "application/html")
    public ModelAndView getLogin() {
        //logger.info("Id to be deleted -> "+id);
        System.out.println("In Login get");
        return new ModelAndView("login");
    }

    @GetMapping(value = "/", produces = "application/html")
    public ModelAndView getIndex() {
        //logger.info("Id to be deleted -> "+id);
        System.out.println("In Index get");
        return new ModelAndView("index");
    }

    /*@GetMapping(value = "/dashboard", produces = "application/html")
    public ModelAndView getHome() {
        //logger.info("Id to be deleted -> "+id);
        System.out.println("In Dashboard get");
        return new ModelAndView("index");
    }*/

    @GetMapping(value = "/error", produces = "application/html")
    public ModelAndView accessError() {
        //logger.info("Id to be deleted -> "+id);
        System.out.println("In Error get");
        return new ModelAndView("error");
    }
}
