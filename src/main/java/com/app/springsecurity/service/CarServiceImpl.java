package com.app.springsecurity.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.springsecurity.entity.Car;
import com.app.springsecurity.repository.CarRepository;

@Service
@Transactional
public class CarServiceImpl implements CarService{

	@Autowired
	CarRepository repo;
	
	@Override
	public ResponseEntity<?> addCar(Car car) {
		try {
			return new ResponseEntity<Integer>(repo.save(car).getCarId(),new HttpHeaders(),HttpStatus.CREATED);
		} catch (Exception e) {
			//logger.error("Error While Persisting Object"+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<?> getCar(Integer id) {
		try {
			Car dbEntity = repo.findOne(id);
			if(null == dbEntity)
				return ResponseEntity.notFound().build();
			return new ResponseEntity<Car>(dbEntity,new HttpHeaders(),HttpStatus.OK);
		}catch(Exception e) {
			//logger.error("No Record Found."+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<?> getAllCars() {
		try {
			System.out.println("Getting all Cars from serviceImpl");
			List<Car> carList=repo.findAll();
			if(carList.isEmpty()) 
				return ResponseEntity.notFound().build();
			return new ResponseEntity<List<Car>>(carList,new HttpHeaders(),HttpStatus.OK);
		}catch(Exception e) {
			//logger.error("No Record Found."+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

}
