package com.app.springsecurity.service;

import com.app.springsecurity.entity.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService {

    ResponseEntity<?> addUser(User user);

    ResponseEntity<?> getUser(Integer id);

    ResponseEntity<?> getAllUsers();

    ResponseEntity<?> addFirstUser(User user);

}
