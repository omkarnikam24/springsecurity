package com.app.springsecurity.service;

import org.springframework.http.ResponseEntity;
import com.app.springsecurity.entity.Person;

public interface PersonService {

	public ResponseEntity<?> addPerson(Person person);
	
	public ResponseEntity<?> getPerson(Integer id);
	
	public Person getPersonEntity(Integer id);
	
	public ResponseEntity<?> getAllPersons();
}
