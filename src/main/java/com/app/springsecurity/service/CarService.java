package com.app.springsecurity.service;

import org.springframework.http.ResponseEntity;
import com.app.springsecurity.entity.Car;


public interface CarService {
	
	public ResponseEntity<?> addCar(Car car);
	
	public ResponseEntity<?> getCar(Integer id);
	
	public ResponseEntity<?> getAllCars();
}
