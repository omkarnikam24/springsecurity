package com.app.springsecurity.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.springsecurity.entity.Person;
import com.app.springsecurity.repository.PersonRepository;

@Service
@Transactional
public class PersonServiceImpl implements PersonService{

	@Autowired
	PersonRepository repo;
	
	@Override
	public ResponseEntity<?> addPerson(Person person) {
		try {
			return new ResponseEntity<Integer>(repo.save(person).getPersonId(),new HttpHeaders(),HttpStatus.CREATED);
		} catch (Exception e) {
			//logger.error("Error While Persisting Object"+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<?> getPerson(Integer id) {
		try {
			Person dbEntity = repo.findOne(id);
			if(null == dbEntity)
				return ResponseEntity.notFound().build();
			return new ResponseEntity<Person>(dbEntity,new HttpHeaders(),HttpStatus.OK);
		}catch(Exception e) {
			//logger.error("No Record Found."+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<?> getAllPersons() {
		try {
			List<Person> personList=repo.findAll();
			if(personList.isEmpty()) 
				return ResponseEntity.notFound().build();
			return new ResponseEntity<List<Person>>(personList,new HttpHeaders(),HttpStatus.OK);
		}catch(Exception e) {
			//logger.error("No Record Found."+e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public Person getPersonEntity(Integer id) {
		try {
			Person dbEntity = repo.findOne(id);
			if(null == dbEntity)
				return null;
			return dbEntity;
		}catch(Exception e) {
			return null;
		}
	}

}
