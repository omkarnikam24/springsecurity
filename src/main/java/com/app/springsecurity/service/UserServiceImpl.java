package com.app.springsecurity.service;

import com.app.springsecurity.entity.User;
import com.app.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public ResponseEntity<?> addUser(User user) {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return new ResponseEntity<Integer>(repo.save(user).getUserId(), new HttpHeaders(), HttpStatus.CREATED);
        } catch (Exception e) {
            //logger.error("Error While Persisting Object"+e.getMessage());
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public ResponseEntity<?> getUser(Integer id) {
        try {
            User dbEntity = repo.findOne(id);
            if (null == dbEntity)
                return ResponseEntity.notFound().build();
            return new ResponseEntity<User>(dbEntity, new HttpHeaders(), HttpStatus.OK);
        } catch (Exception e) {
            //logger.error("No Record Found."+e.getMessage());
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public ResponseEntity<?> getAllUsers() {
        try {
            List<User> userList = repo.findAll();
            if (userList.isEmpty())
                return ResponseEntity.notFound().build();
            return new ResponseEntity<List<User>>(userList, new HttpHeaders(), HttpStatus.OK);
        } catch (Exception e) {
            //logger.error("No Record Found."+e.getMessage());
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public ResponseEntity<?> addFirstUser(User user) {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            System.out.println("Encrypted Password : " + user.getPassword());
            return new ResponseEntity<Integer>(repo.save(user).getUserId(), new HttpHeaders(), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
