package com.app.springsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.springsecurity.entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer>{

}
